# austraeoh

The [*Austraeoh* series](https://www.fimfiction.net/story/25966/austraeoh), by [Imploding Colon](https://www.fimfiction.net/user/32973/Imploding+Colon), is an unfinished series of *My Little Pony: Frendship Is Magic* fanfiction. It bears the simple description, "Rainbow Dash flies east", but is an epic adventure story spanning nine books, two thousand chapters, and over 3.6 million words. And it's still updating!

This repository holds the source code for a [website](https://hyper-diamond.gitlab.io/austraeoh/) that aims to contain interesting things related to the *Austraeoh* series. 

## License?

I don't understand how to deal with licenses correctly. If you know, can you help?

The contents of `resources/books/*` are a copy of the Austraeoh series, downloaded from [FimFiction](https://www.fimfiction.net). As such, they are [IC](https://www.fimfiction.net/user/32973/Imploding+Colon)'s work.

Everything else is either my own work, or auto-generated.

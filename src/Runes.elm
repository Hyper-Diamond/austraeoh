module Runes exposing (austraeoh, eljunbyro, innavedr, odrsjot, urohringr, yaerfaerda, ynanhluutr)

import Element exposing (Element, image)


austraeoh : Element msg
austraeoh =
    image [] { description = "Austraeoh", src = "https://vignette.wikia.nocookie.net/austraeoh/images/4/46/Symbol_austraeoh.png/revision/latest?cb=20150513194132" }


eljunbyro : Element msg
eljunbyro =
    image [] { description = "Eljunbyro", src = "https://vignette.wikia.nocookie.net/austraeoh/images/7/7d/Symbol_eljunbyro.png/revision/latest?cb=20150513194137" }


innavedr : Element msg
innavedr =
    image [] { description = "Innavedr", src = "https://vignette.wikia.nocookie.net/austraeoh/images/f/fb/Symbol_innavedr.png/revision/latest?cb=20150513194144" }


odrsjot : Element msg
odrsjot =
    image [] { description = "Odrsjot", src = "https://vignette.wikia.nocookie.net/austraeoh/images/1/15/Symbol_odrsjot.png/revision/latest?cb=20150513194151" }


urohringr : Element msg
urohringr =
    image [] { description = "Urohringr", src = "https://vignette.wikia.nocookie.net/austraeoh/images/7/73/Symbol_urohringr.png/revision/latest?cb=20150513194157" }


yaerfaerda : Element msg
yaerfaerda =
    image [] { description = "Yaerfaerda", src = "https://vignette.wikia.nocookie.net/austraeoh/images/a/ab/Symbol_yaerfaerda.png/revision/latest?cb=20150513194202" }


ynanhluutr : Element msg
ynanhluutr =
    image [] { description = "Ynanhluutr", src = "https://vignette.wikia.nocookie.net/austraeoh/images/d/d0/Symbol_ynanhluutr.png/revision/latest?cb=20170621043519" }

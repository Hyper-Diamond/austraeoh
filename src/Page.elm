module Page exposing (Model, Msg(..), decodeUrl, init, rerender, update, view)

import Browser
import Element exposing (centerX, column, el, fill, height, row, spacing, width)
import Element.Background as Background
import Element.Font as Font
import Page.Home as Home
import Page.Trends as Trends
import Url
import Url.Parser as Parser exposing (Parser)
import Utils exposing (Window, backgroundColor, foregroundColor, linkColor, toElmUIColor)
import WasmRequest


type Model
    = HomePage
    | TrendsPage Trends.Model


type Msg
    = GotTrendsMsg Trends.Msg


init : Model
init =
    HomePage


view : Window -> WasmRequest.Status -> Model -> Browser.Document Msg
view window status model =
    let
        { title, content } =
            case model of
                HomePage ->
                    Home.view

                TrendsPage m ->
                    let
                        v =
                            Trends.view window status m
                    in
                    { title = v.title
                    , content = Element.map GotTrendsMsg v.content
                    }
    in
    { title = title
    , body =
        [ Element.layout
            [ Background.color <| toElmUIColor backgroundColor
            , Font.color <| toElmUIColor foregroundColor
            , Element.padding 20
            ]
          <|
            row [ width fill ]
                [ column
                    [ width fill, height fill, spacing 20 ]
                    [ el [ Font.size 48, Element.centerX ] (Element.text title)
                    , content
                    ]
                , column [ Element.alignRight, Element.alignTop, Font.color <| toElmUIColor linkColor ]
                    [ Element.link [ centerX ]
                        { url = "/#"
                        , label = Element.text "Home"
                        }
                    , Element.newTabLink [ centerX ]
                        { url = "https://gitlab.com/Hyper-Diamond/austraeoh"
                        , label = Element.text "src"
                        }
                    ]
                ]
        ]
    }


update : Msg -> Model -> ( Model, Bool )
update msg_ model_ =
    case ( msg_, model_ ) of
        ( GotTrendsMsg msg, TrendsPage model ) ->
            let
                ( model2, needsRerender ) =
                    Trends.update msg model
            in
            ( TrendsPage model2, needsRerender )

        _ ->
            ( model_, False )


rerender : Model -> Cmd msg
rerender model =
    case model of
        HomePage ->
            Cmd.none

        TrendsPage m ->
            Trends.rerender m



-- Route


parser : Parser (Model -> a) a
parser =
    Parser.oneOf
        [ Parser.map HomePage Parser.top
        , Parser.map (TrendsPage Trends.init) (Parser.s "trends")
        ]


decodeUrl : Url.Url -> Model
decodeUrl url =
    Parser.parse parser
        { url | path = url.fragment |> Maybe.withDefault "", fragment = Nothing }
        |> Maybe.withDefault HomePage

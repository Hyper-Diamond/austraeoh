mod chapters;
use chapters::SERIES;

use regex::RegexBuilder;

use plotters::prelude::*;

use wasm_bindgen::{prelude::*, JsCast};

fn get(json: &JsValue, key: impl Into<JsValue>) -> JsValue {
    js_sys::Reflect::get(json, &key.into()).unwrap()
}
trait Parse {
    // Panics on parse error, because that is the programmer's fault, not the user.
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self;
}

impl Parse for bool {
    fn parse(json: &JsValue, _: &js_sys::Array) -> Self {
        json.as_bool().unwrap()
    }
}

impl Parse for String {
    fn parse(json: &JsValue, _: &js_sys::Array) -> Self {
        json.as_string().unwrap()
    }
}

impl Parse for f64 {
    fn parse(json: &JsValue, _: &js_sys::Array) -> Self {
        json.as_f64().unwrap()
    }
}

impl Parse for u8 {
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        let x = f64::parse(json, errors);
        assert!(x >= Self::MIN as f64);
        assert!(x <= Self::MAX as f64);
        x as Self
    }
}

impl Parse for isize {
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        let x = f64::parse(json, errors);
        assert!(x >= Self::MIN as f64);
        assert!(x <= Self::MAX as f64);
        x as Self
    }
}

impl Parse for usize {
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        let x = f64::parse(json, errors);
        assert!(x >= Self::MIN as f64);
        assert!(x <= Self::MAX as f64);
        x as Self
    }
}

impl Parse for RGBColor {
    // color = [u8, u8, u8]
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        RGBColor(
            u8::parse(&get(json, 0), errors),
            u8::parse(&get(json, 1), errors),
            u8::parse(&get(json, 2), errors),
        )
    }
}

struct DataSeries {
    start: f64,
    data: Vec<f64>,
}

impl Parse for DataSeries {
    /*
    DataSeries = {
        "type" = "countMatches",
        "regex": string,
        "badRegex": value,
        "chapterRange": [int, int], (inclusive)
        "caseSensitive": bool,
    } | {
        "type" = "blur",
        "blur": int > 0,
        "data": DataSeries,
    }
    */
    #[allow(clippy::range_minus_one)]
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        match &String::parse(&get(json, "type"), errors) as &str {
            "countMatches" => {
                if let Ok(regex) = RegexBuilder::new(&String::parse(&get(json, "regex"), errors))
                    .case_insensitive(!bool::parse(&get(json, "caseSensitive"), errors))
                    .build()
                {
                    let range = get(json, "chapterRange");
                    let min = isize::parse(&get(&range, 0), errors).max(1) as usize;
                    let max = (isize::parse(&get(&range, 1), errors).max(1) as usize)
                        .min(SERIES.chapters.len());

                    let range = min - 1..=max - 1;

                    Self {
                        start: min as f64,
                        data: if range.start() > range.end() {
                            Vec::new()
                        } else {
                            SERIES.chapters[range]
                                .iter()
                                .map(move |chapter| regex.find_iter(chapter.text).count() as f64)
                                .collect()
                        },
                    }
                } else {
                    errors.push(&get(json, "badRegex"));
                    Self {
                        start: 0.,
                        data: Vec::new(),
                    }
                }
            }
            "blur" => {
                let mut data = Self::parse(&get(json, "data"), errors);
                let blur = usize::parse(&get(json, "blur"), errors);
                data.start += (blur - 1) as f64 / 2.;
                data.data = data
                    .data
                    .windows(blur)
                    .map(|w| w.iter().sum::<f64>() / blur as f64)
                    .collect();
                data
            }
            _ => panic!(),
        }
    }
}

struct Plot {
    canvas: CanvasBackend,
    caption: String,

    bg_color: RGBColor,
    fg_color: RGBColor,
    mesh_colors: (RGBColor, RGBColor),

    // Each series is nonempty. But there may be zero series.
    series: Vec<(RGBColor, DataSeries)>,
}

impl Parse for Plot {
    /*
    Plot = {
        "canvasId": valid canvas id (string),
        "caption": string,
        "bgColor": color,
        "fgColor": color,
        "meshColors": [color, color],
        "dataSeries": [{ "color": color, data: DataSeries } , ...]
    }
    */
    fn parse(json: &JsValue, errors: &js_sys::Array) -> Self {
        Self {
            canvas: CanvasBackend::new(&String::parse(&get(json, "canvasId"), errors)).unwrap(),
            caption: String::parse(&get(json, "caption"), errors),
            bg_color: RGBColor::parse(&get(json, "bgColor"), errors),
            fg_color: RGBColor::parse(&get(json, "fgColor"), errors),
            mesh_colors: {
                let colors = get(json, "meshColors");
                (
                    RGBColor::parse(&get(&colors, 0), errors),
                    RGBColor::parse(&get(&colors, 1), errors),
                )
            },
            series: get(json, "dataSeries")
                .dyn_into::<js_sys::Array>()
                .unwrap()
                .iter()
                .map(|json| {
                    (
                        RGBColor::parse(&get(&json, "color"), errors),
                        DataSeries::parse(&get(&json, "data"), errors),
                    )
                })
                .filter(|(_, data)| data.data.len() >= 2)
                .collect(),
        }
    }
}

impl Plot {
    fn draw(self) {
        let root = self.canvas.into_drawing_area();
        root.fill(&self.bg_color).unwrap();

        if self.series.is_empty() {
            // Nothing to graph.
            return;
        }

        let mut x_min = f64::MAX;
        let mut x_max = f64::MIN;
        let mut y_min: f64 = 0.;
        let mut y_max: f64 = 0.;

        for (_, series) in &self.series {
            x_min = x_min.min(series.start);
            x_max = x_max.max(series.start + series.data.len() as f64 - 1.);
            for y in &series.data {
                y_min = y_min.min(*y);
                y_max = y_max.max(*y);
            }
        }

        if y_max == 0. {
            y_max = 1.
        }

        let title_font =
            FontDesc::new(FontFamily::SansSerif, 20., FontStyle::Normal).color(&self.fg_color);
        let axis_font =
            FontDesc::new(FontFamily::Serif, 12., FontStyle::Normal).color(&self.fg_color);

        let mut chart = ChartBuilder::on(&root)
            .caption(&self.caption, title_font)
            .x_label_area_size(30)
            .y_label_area_size(80)
            .build_ranged(x_min..x_max, y_min..y_max)
            .unwrap();

        chart
            .configure_mesh()
            .x_labels(5)
            .y_labels(5)
            .axis_style(&self.fg_color)
            .label_style(axis_font)
            .line_style_1(&self.mesh_colors.0)
            .line_style_2(&self.mesh_colors.1)
            .draw()
            .unwrap();

        for (color, DataSeries { start, data }) in self.series {
            chart
                .draw_series(LineSeries::new(
                    data.into_iter()
                        .enumerate()
                        .map(|(i, x)| (start + i as f64, x)),
                    &color,
                ))
                .unwrap();
        }

        root.present().unwrap();
    }
}

#[wasm_bindgen]
pub fn main(request: &JsValue) -> js_sys::Array {
    let errors = js_sys::Array::new();
    match &String::parse(&get(request, "request"), &errors) as &str {
        "plot" => Plot::parse(request, &errors).draw(),
        _ => panic!(),
    }
    errors
}

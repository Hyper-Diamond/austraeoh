#[derive(Copy, Clone)]
pub struct Series {
    pub title: &'static str,
    pub books: &'static [Book],
    pub chapters: &'static [Chapter],
}

#[derive(Copy, Clone)]
pub struct Book {
    pub title: &'static str,
    pub chapters: &'static [Chapter],
}

#[derive(Copy, Clone)]
pub struct Chapter {
    pub title: &'static str,
    pub text: &'static str,
}

impl Series {
    fn parse(title: &'static str, raw: impl IntoIterator<Item = &'static str>) -> Option<Self> {
        let mut books = Vec::new();
        let mut chapters = Vec::new();

        for raw in raw {
            let start = chapters.len();

            let mut iter = raw.split('>').map(str::trim);
            soft_assert_eq(iter.next()?, "");
            let title = iter.next()?;
            iter.next()?;
            soft_assert_eq(
                iter.next()?,
                "--------------------------------------------------------------------------",
            );

            while let Some(title) = iter.next() {
                let text = iter.next()?.trim_start_matches('-').trim_start();
                chapters.push(Chapter { title, text });
            }

            let end = chapters.len();

            books.push((title, start..end));
        }

        let chapters: &[Chapter] = Box::leak(chapters.into_boxed_slice());

        let books: Vec<Book> = books
            .into_iter()
            .map(|(title, range)| Book {
                title,
                chapters: &chapters[range],
            })
            .collect();

        let books = Box::leak(books.into_boxed_slice());

        Some(Series {
            books,
            chapters,
            title,
        })
    }
}

lazy_static::lazy_static! {
    pub static ref SERIES: Series = Series::parse("Austraeoh", vec![
        include_str!("../resources/books/Austraeoh.txt"),
        include_str!("../resources/books/Eljunbyro.txt"),
        include_str!("../resources/books/Innavedr.txt"),
        include_str!("../resources/books/Odrsjot.txt"),
        include_str!("../resources/books/Urohringr.txt"),
        include_str!("../resources/books/Yaerfaerda.txt"),
        include_str!("../resources/books/Ynanhluutr.txt"),
        include_str!("../resources/books/Utaan.txt"),
        include_str!("../resources/books/Ofolrodi.txt"),
    ]).expect("Failed to parse series");
}

/// assert_eq, returning None on failure.
fn soft_assert_eq<T: core::fmt::Debug + Eq>(a: T, b: T) -> Option<()> {
    if a == b {
        Some(())
    } else {
        None
    }
}

#[cfg(test)]
fn test_book(number: usize, num_chapters: usize, title: &str) {
    let book = &SERIES.books[number - 1];
    assert_eq!(book.title, title);
    assert_eq!(book.chapters.len(), num_chapters);
}

#[test]
fn test() {
    test_book(1, 200, "Austraeoh");
    test_book(2, 200, "Eljunbyro");
    test_book(3, 200, "Innavedr");
    test_book(4, 200, "Odrsjot");
    test_book(5, 200, "Urohringr");
    test_book(6, 200, "Yaerfaerda");
    test_book(7, 200, "Ynanhluutr");
    test_book(8, 300, "Utaan");
    test_book(9, 265, "Ofolrodi");

    assert_eq!(SERIES.books[0].chapters[0].title, "Sunrise");
    assert_eq!(SERIES.books[8].chapters[264].title, "That Which We Purge");

    assert_eq!(SERIES.chapters[0].title, "Sunrise");
    assert_eq!(SERIES.chapters[1964].title, "That Which We Purge");
}

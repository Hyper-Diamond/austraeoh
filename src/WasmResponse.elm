port module WasmResponse exposing (response)

import Json.Decode exposing (Value)


port response : (Value -> msg) -> Sub msg

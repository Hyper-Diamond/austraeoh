port module WasmRequest exposing (DataSeries, Msg, Status(..), blur, countMatches, plot, subscriptions, update)

import Color exposing (Color, rgb)
import Json.Encode as Encode exposing (Value)
import Utils exposing (backgroundColor, encodeColor, foregroundColor)


port request : Value -> Cmd msg


port loaded : ({} -> msg) -> Sub msg


port wasmError : ({} -> msg) -> Sub msg


type Status
    = Loading
    | Ready
    | Failed


type Msg
    = WasmLoaded
    | WasmError


update : Msg -> Status
update msg =
    case msg of
        WasmLoaded ->
            Ready

        WasmError ->
            Failed


subscriptions : Sub Msg
subscriptions =
    Sub.batch [ loaded (\_ -> WasmLoaded), wasmError (\_ -> WasmError) ]



--


plot : { canvasId : String, caption : String, dataSeries : List { color : Color, data : DataSeries } } -> Cmd msg
plot { canvasId, caption, dataSeries } =
    request <|
        Encode.object
            [ ( "request", Encode.string "plot" )
            , ( "canvasId", Encode.string canvasId )
            , ( "caption", Encode.string caption )
            , ( "bgColor", encodeColor backgroundColor )
            , ( "fgColor", encodeColor foregroundColor )
            , ( "meshColors", Encode.list encodeColor [ rgb 0.4 0.5 0.6, rgb 0.2 0.3 0.4 ] )
            , ( "dataSeries"
              , Encode.list Encode.object <|
                    List.map
                        (\{ color, data } ->
                            let
                                (DataSeries d) =
                                    data
                            in
                            [ ( "color", encodeColor color ), ( "data", d ) ]
                        )
                        dataSeries
              )
            ]


type DataSeries
    = DataSeries Value


countMatches : { regex : String, badRegex : Value, chapterRange : ( Int, Int ), caseSensitive : Bool } -> Maybe DataSeries
countMatches { regex, badRegex, chapterRange, caseSensitive } =
    if String.isEmpty regex then
        Nothing

    else
        Just <|
            DataSeries <|
                Encode.object
                    [ ( "type", Encode.string "countMatches" )
                    , ( "regex", Encode.string regex )
                    , ( "badRegex", badRegex )
                    , ( "chapterRange", Encode.list Encode.int [ Tuple.first chapterRange, Tuple.second chapterRange ] )
                    , ( "caseSensitive", Encode.bool caseSensitive )
                    ]


blur : Int -> DataSeries -> Maybe DataSeries
blur n (DataSeries data) =
    if n >= 1 then
        Just <|
            DataSeries <|
                Encode.object
                    [ ( "type", Encode.string "blur" )
                    , ( "blur", Encode.int n )
                    , ( "data", data )
                    ]

    else
        Nothing

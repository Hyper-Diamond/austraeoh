module Utils exposing (..)

import Color exposing (Color, rgb)
import Element exposing (Element, el, height, px, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html
import Html.Attributes
import Json.Encode as Encode exposing (Value)


toElmUIColor : Color -> Element.Color
toElmUIColor color =
    let
        { red, green, blue, alpha } =
            Color.toRgba color
    in
    Element.rgba red green blue alpha


encodeColor : Color -> Value
encodeColor color =
    let
        { red, green, blue } =
            Color.toRgba color
    in
    Encode.list (Encode.int << floor << (*) 255) [ red, green, blue ]


backgroundColor : Color
backgroundColor =
    rgb 0.1 0.0 0.3


foregroundColor : Color
foregroundColor =
    rgb 0.8 0.5 0.2


linkColor : Color
linkColor =
    rgb 0.4 0.5 0.6


inputColor : Color
inputColor =
    rgb 0.2 0.4 0.6


inputErrorColor : Color
inputErrorColor =
    rgb 0.6 0.4 0.2


input : { error : Bool, label : String, msg : String -> msg, text : String } -> Element msg
input { error, label, msg, text } =
    Input.text
        (inputAttrs error)
        { onChange = msg
        , placeholder = Nothing
        , text = text
        , label = Input.labelAbove [ Element.centerX ] (Element.text label)
        }


inputAttrs : Bool -> List (Element.Attribute msg)
inputAttrs error =
    [ Background.color <|
        toElmUIColor
            (if error then
                inputErrorColor

             else
                inputColor
            )
    , Font.color <| toElmUIColor (rgb 0 0 0)
    ]


button : msg -> String -> Element msg
button msg label =
    Input.button
        [ Background.color <| toElmUIColor inputColor
        , Border.rounded 10
        ]
        { onPress = Just msg
        , label = Element.text label
        }


canvas : String -> ( Int, Int ) -> Element msg
canvas id ( w, h ) =
    el
        [ width (px w)
        , height (px h)
        ]
        (Element.html
            (Html.canvas
                [ Html.Attributes.id id
                , Html.Attributes.width w
                , Html.Attributes.height h
                ]
                []
            )
        )


type alias Window =
    { width : Int
    , height : Int
    }

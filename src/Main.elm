module Main exposing (main)

import Browser
import Browser.Events
import Browser.Navigation as Nav
import Html
import Page
import Page.Trends
import Url
import Utils exposing (Window)
import WasmRequest exposing (Status(..))
import WasmResponse


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }


type alias Flags =
    { window : Window
    }


type alias Model =
    { key : Nav.Key
    , window : Window
    , page : Page.Model
    , wasmStatus : Status
    }


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init { window } url key =
    let
        page =
            Page.decodeUrl url
    in
    ( { key = key
      , window = window
      , page = page
      , wasmStatus = Loading
      }
    , Page.rerender page |> Cmd.map GotPageMsg
    )


type Msg
    = GotPageMsg Page.Msg
    | UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url
    | GotWasmStatusUpdate WasmRequest.Msg
    | WindowResized Window


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotPageMsg pageMsg ->
            let
                ( page, needsRerender ) =
                    Page.update pageMsg model.page
            in
            ( { model | page = page }
            , if needsRerender then
                Page.rerender page |> Cmd.map GotPageMsg

              else
                Cmd.none
            )

        GotWasmStatusUpdate wasmMsg ->
            ( { model | wasmStatus = WasmRequest.update wasmMsg }, Page.rerender model.page )

        WindowResized win ->
            ( { model | window = win }, Page.rerender model.page )

        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                page =
                    Page.decodeUrl url
            in
            ( { model | page = page }
            , Page.rerender page
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ WasmResponse.response (\value -> GotPageMsg <| Page.GotTrendsMsg <| Page.Trends.GotResponse value)
        , WasmRequest.subscriptions |> Sub.map GotWasmStatusUpdate
        , Browser.Events.onResize (\w h -> WindowResized { width = w, height = h })
        ]


view : Model -> Browser.Document Msg
view model =
    let
        { title, body } =
            Page.view model.window model.wasmStatus model.page
    in
    { title = title, body = List.map (Html.map GotPageMsg) body }



{-

   module Main exposing (main)

   import Browser
   import Browser.Events
   import Element exposing (Element, column, el, fill, height, none, padding, px, row, shrink, spacing, width)
   import Element.Background as Background
   import Element.Font as Font
   import Json.Decode as Decode exposing (Value)
   import Page.Trends exposing (..)
   import Set
   import Url exposing (Url)
   import Url.Parser
   import Utils exposing (..)
   import WasmRequest
   import WasmResponse exposing (response)


   type alias Flags =
       { window : Window
       }


   main : Program Flags Model Msg
   main =
       Browser.document
           { init =
               \flags ->
                   let
                       model =
                           init flags
                   in
                   ( model, cmd model )
           , update =
               \msg model ->
                   let
                       ( model2, sendCmd ) =
                           update msg model
                   in
                   ( model2
                   , if sendCmd then
                       cmd model2

                     else
                       Cmd.none
                   )
           , view =
               \model ->
                   { title = "Austraeoh Trends"
                   , body =
                       [ Element.layout
                           [ Background.color <| toElmUIColor backgroundColor
                           , Font.color <| toElmUIColor foregroundColor
                           , padding 20
                           ]
                           (view model)
                       ]
                   }
           , subscriptions =
               \_ ->
                   Sub.batch
                       [ Browser.Events.onResize WindowResized
                       , response GotResponse
                       , Sub.map GotWasmMsg WasmRequest.subscriptions
                       ]
           }



   -- Model


   type alias Model =
       { settings : Settings
       , queries : Queries
       , window : Window
       , regexErrors : Set.Set Int
       , loadStatus : WasmRequest.Status
       }


   type Msg
       = ChangedSettings SettingsMsg
       | ChangedQueries QueriesMsg
       | WindowResized Int Int
       | GotResponse Value
       | GotWasmMsg WasmRequest.Msg


   init : Flags -> Model
   init flags =
       { settings = settingsInit
       , queries = queriesInit
       , window = flags.window
       , regexErrors = Set.empty
       , loadStatus = WasmRequest.Loading
       }


   view : Model -> Element Msg
   view model =
       column [ width fill, height fill, spacing 20 ]
           [ row [ width fill ]
               [ el [ Font.size 48, Element.centerX ] (Element.text "Austraeoh Trends")
               , Element.newTabLink
                   [ Element.alignRight
                   , Element.alignTop
                   , Font.color <| toElmUIColor linkColor
                   ]
                   { url = "https://gitlab.com/Hyper-Diamond/austraeoh"
                   , label = Element.text "src"
                   }
               ]
           , row [ spacing 20, height fill ]
               [ column [ width fill, height fill, spacing 20 ]
                   [ Element.map ChangedSettings <| settingsView model.settings
                   , case model.loadStatus of
                       WasmRequest.Ready _ ->
                           canvas "trends-plot" ( model.window.width // 2, model.window.height // 2 )

                       WasmRequest.Loading ->
                           Element.paragraph [ Font.size 40 ]
                               [ Element.text "Loading the Austraeoh Series. (This may take a while.)" ]

                       WasmRequest.Failed ->
                           Element.paragraph [ Font.size 40 ]
                               [ Element.text "Your browser does not appear to support WebAssembly." ]
                   ]
               , el [ width fill, Element.alignTop ]
                   (Element.map ChangedQueries
                       (queriesView
                           (\n -> Set.member n model.regexErrors)
                           model.queries
                       )
                   )
               ]
           ]


   update : Msg -> Model -> ( Model, Bool )
   update msg model =
       case msg of
           ChangedSettings settingsMsg ->
               ( { model | settings = settingsUpdate settingsMsg model.settings }, True )

           ChangedQueries queriesMsg ->
               ( { model | queries = queriesUpdate queriesMsg model.queries }, True )

           WindowResized w h ->
               ( { model | window = { width = w, height = h } }, True )

           GotResponse value ->
               ( case Decode.decodeValue (Decode.list Decode.int) value of
                   Err _ ->
                       model

                   Ok errors ->
                       { model | regexErrors = Set.fromList errors }
               , False
               )

           GotWasmMsg wasmMsg ->
               ( { model | loadStatus = WasmRequest.update wasmMsg }, True )


   cmd : Model -> Cmd msg
   cmd model =
       case model.loadStatus of
           WasmRequest.Ready key ->
               WasmRequest.wasm key <|
                   WasmRequest.plot
                       { canvasId = "trends-plot"
                       , caption = "Trends"
                       , dataSeries =
                           case settingsRequest model.settings of
                               Nothing ->
                                   []

                               Just { chapterRange, blur } ->
                                   List.filterMap
                                       (\data -> WasmRequest.blur blur data.data |> Maybe.map (\d -> { data | data = d }))
                                       (queriesRequest chapterRange model.queries)
                       }

           _ ->
               Cmd.none



   -- Route


   type Route
       = Home


   fromUrl : Url -> Maybe Route
   fromUrl url =
       -- Url.Parser.parse parser { url | path = url.fragment |> Maybe.withDefault "", fragment = Nothing }
       Just Home

-}

module Page.Trends exposing (Model, Msg(..), init, rerender, update, view)

import Array exposing (Array)
import Color exposing (Color)
import ColorPicker
import Element exposing (Element, column, el, fill, height, none, px, row, shrink, spacing, width)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Json.Decode as Decode
import Json.Encode as Encode exposing (Value)
import Set exposing (Set)
import Utils exposing (Window, button, canvas, foregroundColor, input, inputAttrs, toElmUIColor)
import WasmRequest exposing (DataSeries)


type alias Model =
    { settings : Settings
    , queries : Queries
    , regexErrors : Set Int
    }


type Msg
    = ChangedSettings SettingsMsg
    | ChangedQueries QueriesMsg
    | GotResponse Value


init : Model
init =
    { settings = settingsInit
    , queries = queriesInit
    , regexErrors = Set.empty
    }


update : Msg -> Model -> ( Model, Bool )
update msg model =
    case msg of
        ChangedSettings settingsMsg ->
            ( { model | settings = settingsUpdate settingsMsg model.settings }, True )

        ChangedQueries queriesMsg ->
            ( { model | queries = queriesUpdate queriesMsg model.queries }, True )

        GotResponse value ->
            ( case Decode.decodeValue (Decode.list Decode.int) value of
                Err _ ->
                    model

                Ok errors ->
                    { model | regexErrors = Set.fromList errors }
            , False
            )


rerender : Model -> Cmd msg
rerender model =
    WasmRequest.plot
        { canvasId = "trends-plot"
        , caption = "Trends"
        , dataSeries =
            case settingsRequest model.settings of
                Nothing ->
                    []

                Just { chapterRange, blur } ->
                    List.filterMap
                        (\data -> WasmRequest.blur blur data.data |> Maybe.map (\d -> { data | data = d }))
                        (queriesRequest chapterRange model.queries)
        }


view : Window -> WasmRequest.Status -> Model -> { title : String, content : Element Msg }
view window status model =
    { title = "Austraeoh Trends"
    , content =
        row [ spacing 20, height fill ]
            [ column [ width fill, height fill, spacing 20 ]
                [ Element.map ChangedSettings <| settingsView model.settings
                , case status of
                    WasmRequest.Ready ->
                        canvas "trends-plot" ( window.width // 2, window.height // 2 )

                    WasmRequest.Loading ->
                        Element.paragraph [ Font.size 40 ]
                            [ Element.text "Loading the Austraeoh Series. (This may take a while.)" ]

                    WasmRequest.Failed ->
                        Element.paragraph [ Font.size 40 ]
                            [ Element.text "Your browser does not appear to support WebAssembly." ]
                ]
            , el [ width fill, Element.alignTop ]
                (Element.map ChangedQueries
                    (queriesView
                        (\n -> Set.member n model.regexErrors)
                        model.queries
                    )
                )
            ]
    }



-- Settings


type alias Settings =
    { chapterMin : String
    , chapterMax : String
    , blur : String
    }


type SettingsMsg
    = ChapterMinChanged String
    | ChapterMaxChanged String
    | BlurChanged String


settingsInit : Settings
settingsInit =
    { chapterMin = "1"
    , chapterMax = "2000"
    , blur = "5"
    }


settingsView : Settings -> Element SettingsMsg
settingsView settings =
    let
        ( error1, error2, error3 ) =
            case ( String.toInt settings.chapterMin, String.toInt settings.chapterMax, String.toInt settings.blur ) of
                ( Just min, Just max, blur ) ->
                    let
                        invalidRange =
                            min >= max
                    in
                    ( invalidRange
                    , invalidRange
                    , blur
                        |> Maybe.map (\b -> not invalidRange && (max - min) - (b - 1) <= 0)
                        |> Maybe.withDefault True
                    )

                ( min, max, blur ) ->
                    ( min |> Maybe.map (\_ -> False) |> Maybe.withDefault True
                    , max |> Maybe.map (\_ -> False) |> Maybe.withDefault True
                    , blur |> Maybe.map (\n -> n <= 0) |> Maybe.withDefault True
                    )
    in
    row [ spacing 10, Element.centerX ]
        [ input
            { error = error1
            , label = "Chapter Min"
            , msg = ChapterMinChanged
            , text = settings.chapterMin
            }
        , input
            { error = error2
            , label = "Chapter Max"
            , msg = ChapterMaxChanged
            , text = settings.chapterMax
            }
        , input
            { error = error3
            , label = "Blur"
            , msg = BlurChanged
            , text = settings.blur
            }
        ]


settingsUpdate : SettingsMsg -> Settings -> Settings
settingsUpdate msg settings =
    case msg of
        ChapterMinChanged str ->
            { settings | chapterMin = str }

        ChapterMaxChanged str ->
            { settings | chapterMax = str }

        BlurChanged str ->
            { settings | blur = str }


settingsRequest : Settings -> Maybe { chapterRange : ( Int, Int ), blur : Int }
settingsRequest settings =
    Maybe.map3
        (\min max blur ->
            { chapterRange = ( min, max )
            , blur = blur
            }
        )
        (String.toInt settings.chapterMin)
        (String.toInt settings.chapterMax)
        (String.toInt settings.blur)



-- Queries


type alias Queries =
    Array Query


type alias Query =
    { regex : String
    , color : Color
    , picker : Maybe ColorPicker.State
    , caseSensitive : Bool
    }


type QueriesMsg
    = ChangedQuery Int QueryMsg
    | CreatedQuery
    | DeletedQuery


type QueryMsg
    = ChangedRegex String
    | ChangedColor ColorPicker.Msg
    | OpenedPicker
    | ClosedPicker
    | SetCase Bool


queriesInit : Queries
queriesInit =
    Array.repeat 1 { regex = "Rainbow", color = Color.red, picker = Nothing, caseSensitive = True }


queriesView : (Int -> Bool) -> Queries -> Element QueriesMsg
queriesView regexError qs =
    column [ spacing 10 ]
        [ Element.indexedTable [ spacing 10 ]
            { data = Array.toList qs
            , columns =
                [ { header = el [ width fill ] <| el [ Element.centerX ] <| Element.text "Regex"
                  , width = fill
                  , view =
                        \n q ->
                            Element.map (ChangedQuery n) <|
                                Input.text
                                    (inputAttrs (regexError n))
                                    { onChange = ChangedRegex
                                    , placeholder = Nothing
                                    , text = q.regex
                                    , label = Input.labelHidden "Regex"
                                    }
                  }
                , { header = el [ width fill ] <| el [ Element.centerX ] <| Element.text "Color"
                  , width = shrink
                  , view =
                        \n q ->
                            Element.map (ChangedQuery n) <|
                                Input.button
                                    [ Background.color <| toElmUIColor q.color
                                    , height (px 20)
                                    , Border.rounded 10
                                    , Element.below
                                        (case q.picker of
                                            Nothing ->
                                                none

                                            Just picker ->
                                                Element.map ChangedColor <|
                                                    Element.html <|
                                                        ColorPicker.view q.color picker
                                        )
                                    , Events.onLoseFocus ClosedPicker
                                    , Element.centerX
                                    , Element.centerY
                                    ]
                                    { onPress = Just OpenedPicker
                                    , label = el [] none
                                    }
                  }
                , { header = column [ width fill ] <| List.map (el [ Element.centerX ] << Element.text) [ "Case", "Sensitive" ]
                  , width = shrink
                  , view =
                        \n q ->
                            el [ height fill ] <|
                                Element.map (ChangedQuery n) <|
                                    Input.checkbox
                                        [ width shrink
                                        , height shrink
                                        , Element.centerX
                                        , Element.centerY
                                        , Background.color (toElmUIColor foregroundColor)
                                        ]
                                        { onChange = SetCase
                                        , icon = Input.defaultCheckbox
                                        , checked = q.caseSensitive
                                        , label = Input.labelHidden "case sensitive"
                                        }
                  }
                ]
            }
        , row [ spacing 10 ]
            [ if Array.isEmpty qs then
                none

              else
                button DeletedQuery " - "
            , button CreatedQuery " + "
            ]
        ]


queriesRequest : ( Int, Int ) -> Queries -> List { color : Color, data : DataSeries }
queriesRequest chapterRange qs =
    List.filterMap identity <|
        Array.toList <|
            Array.indexedMap
                (\i q ->
                    Maybe.map (\data -> { color = q.color, data = data }) <|
                        WasmRequest.countMatches
                            { regex = q.regex
                            , badRegex = Encode.int i
                            , caseSensitive = q.caseSensitive
                            , chapterRange = chapterRange
                            }
                )
                qs


queriesUpdate : QueriesMsg -> Queries -> Queries
queriesUpdate msg qs =
    case msg of
        ChangedQuery n queryMsg ->
            edit n (queryUpdate queryMsg) qs

        CreatedQuery ->
            Array.push { regex = "", color = Color.red, picker = Nothing, caseSensitive = True } qs

        DeletedQuery ->
            Array.slice 0 -1 qs


queryUpdate : QueryMsg -> Query -> Query
queryUpdate msg q =
    case msg of
        ChangedRegex r ->
            { q | regex = r }

        ChangedColor m ->
            case q.picker of
                Nothing ->
                    q

                Just p ->
                    let
                        ( picker, color ) =
                            ColorPicker.update m q.color p
                    in
                    { q | picker = Just picker, color = color |> Maybe.withDefault q.color }

        OpenedPicker ->
            { q | picker = Just ColorPicker.empty }

        ClosedPicker ->
            { q | picker = Nothing }

        SetCase x ->
            { q | caseSensitive = x }



-- Utils


edit : Int -> (a -> a) -> Array a -> Array a
edit n f a =
    case Array.get n a of
        Nothing ->
            a

        Just x ->
            Array.set n (f x) a

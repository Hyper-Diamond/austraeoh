module Page.Home exposing (view)

import Element exposing (Element, column, el, fill, height, link, maximum, newTabLink, none, spacing, table, width)
import Element.Background as Background
import Element.Font as Font
import Runes
import Utils exposing (linkColor, toElmUIColor)


view : { title : String, content : Element msg }
view =
    { title = "Austraeoh"
    , content =
        column [ Element.centerX, spacing 20 ]
            [ el [ Element.centerX, Font.color (toElmUIColor linkColor), Font.italic ] (Element.text "Rainbow Dash flies east.")
            , table [ Background.color (Element.rgb 1 1 1), Font.color (Element.rgb 0 0 0), width (fill |> maximum 500) ]
                { data =
                    [ { a = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/25966/austraeoh", label = Runes.austraeoh }
                      , b = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/57450/eljunbyro", label = Runes.eljunbyro }
                      , c = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/88713/innavedr", label = Runes.innavedr }
                      , d = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/126689/odrsjot", label = Runes.odrsjot }
                      }
                    , { a = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/171718/urohringr", label = Runes.urohringr }
                      , b = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/213020/yaerfaerda", label = Runes.yaerfaerda }
                      , c = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/254555/ynanhluutr", label = Runes.ynanhluutr }
                      , d = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/290208/utaan", label = el [ Element.centerX, Element.centerY ] (Element.text "Utaan") }
                      }
                    , { a = newTabLink [ width fill, height fill ] { url = "https://www.fimfiction.net/story/373584/ofolrodi", label = el [ Element.centerX, Element.centerY ] (Element.text "Ofolrodi") }
                      , b = none
                      , c = none
                      , d = none
                      }
                    ]
                , columns = List.map (\v -> { header = none, width = fill, view = v }) [ .a, .b, .c, .d ]
                }
            , newTabLink [ Element.centerX ] { label = Element.text "Trailer", url = "https://www.youtube.com/watch?v=18tWAkwuOqc" }
            , link [ Element.centerX ] { label = Element.text "Trends", url = "/#/trends" }
            , newTabLink [ Element.centerX ] { label = Element.text "Wiki", url = "https://austraeoh.fandom.com/wiki/Austraeoh_Wiki" }
            ]
    }

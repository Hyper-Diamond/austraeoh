elm make src/Main.elm --optimize --output=public/elm.js
wasm-pack build --target web --out-dir public

rm public/.gitignore

gzip -kf -9 public/austraeoh_bg.wasm

(cd public && python3 -m http.server)